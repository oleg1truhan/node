const express = require('express');
const fs = require('fs');
const PORT = process.env.PORT || 8080;
const app = express();
const path = require('path');
const validExtension = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
const router = express.Router();

app.use(express.json());

const folderName = './api/files'

const existFolder = () => {
    if (!fs.existsSync(folderName)) {
        fs.mkdirSync(folderName, {recursive: true});
    }
}

existFolder();

const createFile = (req, res, next) => {
    if (fs.existsSync(path.join(folderName, req.body.filename)) || !validExtension.includes(path.extname(req.body.filename))) {
        res.status(400).json({
            message: "Please specify 'content' parameter"
        });
        console.error('Please specify "content" parameter');
    }else {
        try {
            fs.writeFileSync(path.join(folderName, req.body.filename), req.body.content);
            console.log('File created successfully');
            console.log({
                filename: req.body.filename,
                content: req.body.content
            })
            res.status(200).json({
                message: "File created successfully"
            });
        } catch (err) {
            err = 'Server error';
            console.error(err);
            res.status(500).json({
                message: "Server error"
            });
        }
    }

    next();
}

router.post('/', createFile);

const getFiles = (req, res, next) => {
    let files = [];
    try {
        fs.readdirSync(path.join(folderName)).forEach(file => {
            files.push(file);
        });
        if (!files) {
            console.error('Client error');
            res.status(400).json({message: 'Client error'});
        }
        console.log('Success');
        console.log(files);
        res.status(200).json({
            message: 'Success',
            files
        });
    } catch (err) {
        err = 'Server error';
        console.error(err);
        res.status(500).json({
            message: "Server error"
        });
    }

    next();
}

router.get('/', getFiles);

const getFile = (req, res) => {
    try {
        const data = fs.readFileSync(path.join(folderName, req.params.filename), 'utf8');
        console.log('Success');
        res.status(200).json({
            message: "Success",
            filename: req.params.filename,
            content: data,
            extension: path.extname(req.params.filename),
            uploadedDate: new Date()
        });
        console.log({
            message: "Success",
            filename: req.params.filename,
            content: req.params.content,
            extension: path.extname(req.params.filename),
            uploadedDate: new Date()
        });
    } catch (err) {
        if (err.code === 'ENOENT') {
            err = `No file with ${req.body.filename} filename found`;
            console.error(err);
            res.status(400).json({"message": `No file with ${req.params.filename} filename found`});
        } else {
            err = 'Server error';
            console.error(err);
            res.status(500).json({
                message: "Server error"
            });
        }
    }

    res.end();
}

router.get('/:filename', getFile);

app.use('/api/files', router)

app.listen(PORT, () => {
    console.log('Server has been started');
});
